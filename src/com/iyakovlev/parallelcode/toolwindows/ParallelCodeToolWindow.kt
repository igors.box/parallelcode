package com.iyakovlev.parallelcode.toolwindows

import com.intellij.openapi.ui.Messages
import com.iyakovlev.parallelcode.plugincore.languagesupport.MethodEntity
import com.iyakovlev.parallelcode.plugincore.languagesupport.ClassEntity
import com.iyakovlev.parallelcode.plugincore.PluginServices
import com.iyakovlev.parallelcode.plugincore.interop.RemoteId
import com.iyakovlev.parallelcode.plugincore.interop.RemoteServer
import javax.swing.tree.DefaultMutableTreeNode
import javax.swing.tree.DefaultTreeModel
import javax.swing.tree.MutableTreeNode

private const val PLUGIN_MESSAGE_BOX_TITLE = "ParallelCode"

class TestToolWindow(private val pluginServices: PluginServices) {

    private enum class MethodNodeStatus{ OnlyLocal, OnlyRemote, Both, }

    private class EntityTreeNodeForClass(val associatedLocal : ClassEntity?, val associatedRemote: ClassEntity?)
        : DefaultMutableTreeNode(associatedLocal?.name ?: associatedRemote?.name ?: throw IllegalStateException(), true)

    private class EntityTreeNodeForMethod(val associatedMethod : MethodEntity, val status : MethodNodeStatus, text : String) : DefaultMutableTreeNode(text, false)

    private val toolWindow = ParallelCodeToolWindowProxy()
    private val buttonSync = toolWindow.buttonSync!!
    private val exampleTree = toolWindow.exampleTree!!
    private val comboBoxTagSelector = toolWindow.comboBoxTagSelector!!
    private val buttonRefresh = toolWindow.buttonRefresh!!
    private val buttonInsertSelected = toolWindow.buttonInsertSelected!!
    private val buttonGotoSelected = toolWindow.buttonGotoSelected!!

    val panel = toolWindow.panel!!

    init {
        exampleTree.model = null
        buttonSync.addActionListener { onSync() }
        buttonRefresh.addActionListener { onRefresh() }
        buttonInsertSelected.addActionListener { onInsert() }
        buttonGotoSelected.addActionListener { onGoto() }
    }

    private fun onGoto() {
        val selectedNode = exampleTree.lastSelectedPathComponent
        val element = when (selectedNode) {
            is EntityTreeNodeForClass -> selectedNode.associatedLocal?.element
            is EntityTreeNodeForMethod -> selectedNode.associatedMethod.element
            else -> null
        }

        if(element != null){
            pluginServices.goto(element)
        }else{
            Messages.showMessageDialog("Please select local class or method.", PLUGIN_MESSAGE_BOX_TITLE, null)
        }
    }

    private fun insertClass(entity : EntityTreeNodeForClass){
        val associatedRemoteClass = entity.associatedRemote

        if(associatedRemoteClass != null){
            pluginServices.insertCodeEntity(associatedRemoteClass)
        }else{
            Messages.showMessageDialog("This class is not missed here.", PLUGIN_MESSAGE_BOX_TITLE, null)
        }
    }

    private fun insertMethod(entity : EntityTreeNodeForMethod){
        if(entity.status == MethodNodeStatus.OnlyRemote){
            (entity.parent as? EntityTreeNodeForClass)?.associatedLocal?.element?.let {
                pluginServices.insertCodeEntity(it, entity.associatedMethod)
                onSync()
            }
        }
    }

    private fun onInsert() {
        val selectedNode = exampleTree.lastSelectedPathComponent
        when (selectedNode) {
            is EntityTreeNodeForClass -> insertClass(selectedNode)
            is EntityTreeNodeForMethod -> insertMethod(selectedNode)
            else -> Messages.showMessageDialog("Please select missing class or method.", PLUGIN_MESSAGE_BOX_TITLE, null)
        }
    }

    private fun onRefresh(){
        comboBoxTagSelector.removeAllItems()
        for(id in RemoteServer.remoteIds){
            comboBoxTagSelector.addItem(id)
        }
    }

    private fun mergeMethods(local : List<MethodEntity>, remote : List<MethodEntity>) : Iterable<MutableTreeNode>{

        fun getEntityString(entity : MethodEntity) =
                if (entity.parameters.isNotEmpty())
                    entity.parameters.reduce { acc, s -> "$acc, $s" }.removePrefix(", ").let { "${entity.name}($it)" }
                else entity.name

        val result = mutableSetOf<MutableTreeNode>()

        val entityStrings = local.map { getEntityString(it) }.union( remote.map { getEntityString(it) })

        for(entityString in entityStrings){

            val localMethod = local.firstOrNull { getEntityString(it) == entityString }
            val remoteMethod = remote.firstOrNull { getEntityString(it) == entityString }

            val node = if(localMethod != null){
                if(remoteMethod != null){
                    EntityTreeNodeForMethod(localMethod, MethodNodeStatus.Both, entityString)
                }else {
                    EntityTreeNodeForMethod(localMethod, MethodNodeStatus.OnlyLocal, "$entityString -> Excess")
                }
            }else{
                if(remoteMethod != null){
                    EntityTreeNodeForMethod(remoteMethod, MethodNodeStatus.OnlyRemote, "$entityString -> Missed")
                }else{
                    throw IllegalStateException() /*IS NOT POSSIBLE!*/
                }
            }

            result.add(node)
        }

        return result
    }

    private fun mergeToModel(local : List<ClassEntity>, remote : List<ClassEntity>) : Iterable<MutableTreeNode>{

        val result = mutableSetOf<MutableTreeNode>()

        val names = local.map {it.name}.union(remote.map { it.name })

        for(clazzName in names){

            val localClass = local.firstOrNull { it.name == clazzName }
            val remoteClass = remote.firstOrNull { it.name == clazzName }

            val localClazzMethods = localClass?.methods ?: listOf()
            val remoteClazzMethods = remoteClass?.methods ?: listOf()

            val mergedMethods = mergeMethods(localClazzMethods, remoteClazzMethods)

            val clazz = EntityTreeNodeForClass(localClass, remoteClass)

            for(method in mergedMethods){
                clazz.add(method)
            }

            result.add(clazz)
        }

        return result
    }


    private fun onSync(){

        val selectedItem = comboBoxTagSelector.selectedItem as? RemoteId

        if(selectedItem == null){
            Messages.showMessageDialog("Please select project to sync.", "ParallelCode", null)
            return
        }

        val model = pluginServices.modalOperation { statusSetter ->

            statusSetter("Getting remote data...")
            val remoteData = RemoteServer.getRemoteData(selectedItem)

            statusSetter("Getting local data...")
            val localData = pluginServices.projectManipulator.retrieveLocalData()

            statusSetter("Building model...")
             DefaultMutableTreeNode("Project Root").also{
                 for(child in mergeToModel(localData, remoteData)){
                     it.add(child)
                 }
             }
        }

        exampleTree.model = DefaultTreeModel(model)
    }
}
