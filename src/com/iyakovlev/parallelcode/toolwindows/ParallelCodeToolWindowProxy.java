package com.iyakovlev.parallelcode.toolwindows;

import com.iyakovlev.parallelcode.plugincore.interop.RemoteId;

import javax.swing.*;

public class ParallelCodeToolWindowProxy {
    private JButton buttonSync;
    private JTree exampleTree;
    private JPanel panel;
    private JComboBox<RemoteId> comboBoxTagSelector;
    private JButton buttonRefresh;
    private JButton buttonInsertSelected;
    private JButton buttonGoto;

    public JButton getButtonSync() {
        return buttonSync;
    }

    public JButton getButtonInsertSelected() {
        return buttonInsertSelected;
    }

    public JTree getExampleTree() {
        return exampleTree;
    }

    public JPanel getPanel() {
        return panel;
    }

    public JButton getButtonRefresh() { return buttonRefresh; }

    public JComboBox<RemoteId> getComboBoxTagSelector() { return comboBoxTagSelector; }

    public JButton getButtonGotoSelected() {
        return buttonGoto;
    }
}
