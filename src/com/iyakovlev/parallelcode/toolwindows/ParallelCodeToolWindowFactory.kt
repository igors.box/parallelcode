package com.iyakovlev.parallelcode.toolwindows

import com.iyakovlev.parallelcode.plugincore.PluginServices
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowFactory
import com.intellij.ui.content.ContentFactory

class TestToolWindowFactory : ToolWindowFactory {

    override fun createToolWindowContent(p0: Project, p1: ToolWindow) {

        val pluginServices = PluginServices(p0)

        val tool = TestToolWindow(pluginServices)

        val contentFactory = ContentFactory.SERVICE.getInstance()
        val content = contentFactory.createContent(tool.panel, "", false)
        p1.contentManager.addContent(content)
    }

}