package com.iyakovlev.parallelcode.plugincore

import com.intellij.lang.Language
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.project.Project
import com.intellij.openapi.roots.ProjectFileIndex
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import com.intellij.psi.PsiManager
import com.iyakovlev.parallelcode.plugincore.languagesupport.*

class DataManipulator(private val project : Project){

    private val languageServices = listOf(KotlinService(), PythonService())

    private fun getServiceFor(psiFile: PsiElement) : LanguageService? {
        if(Language.getRegisteredLanguages().contains(psiFile.language)){
            return languageServices.firstOrNull { service -> service.language == psiFile.language.id }
        }
        return null
    }

    fun generateClass(entity : ClassEntity, psiFile: PsiFile) : PsiElement?
            = getServiceFor(psiFile)?.generateClass(entity, project)

    fun generateMethod(entity : MethodEntity, psiElement: PsiElement)
            = getServiceFor(psiElement)?.generateMethod(entity, project, psiElement)

    fun retrieveLocalData() : List<ClassEntity> =
            ApplicationManager.getApplication().runReadAction<List<ClassEntity>>{

                val result = mutableListOf<ClassEntity>()

                ProjectFileIndex.SERVICE.getInstance(project).iterateContent {
                    val psiFile = PsiManager.getInstance(project).findFile(it)
                    if(psiFile != null) {
                        result.addAll(getServiceFor(psiFile)?.findAllClasses(psiFile) ?: listOf())
                    }
                    true
                }
                result
            }
}