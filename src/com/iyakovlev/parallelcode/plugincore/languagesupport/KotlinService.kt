package com.iyakovlev.parallelcode.plugincore.languagesupport

import com.intellij.openapi.project.Project
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import com.intellij.psi.util.PsiTreeUtil
import org.jetbrains.kotlin.psi.*

class KotlinService : LanguageService {

    private fun List<String>.parametersToString() =
            if (isNotEmpty()) reduce { acc, s -> "$acc : TYPE, $s" } + " : TYPE" else ""

    override fun generateMethod(entity: MethodEntity, project: Project, into: PsiElement) {

        into as KtClass

        val newMethod = KtPsiFactory(project)
                .createFunction("fun ${entity.name}(${entity.parameters.parametersToString()}) : RETURN_TYPE {}")

        val body = into.getOrCreateBody()

        body.addBefore(newMethod, body.rBrace)
    }

    override fun generateClass(entity: ClassEntity, project : Project): PsiElement {
        val classBuilder = StringBuilder()
        classBuilder.append("class ${entity.name} {")

        for(method in entity.methods){
            classBuilder.append("fun ${method.name}(${method.parameters.parametersToString()}) : RETURN_TYPE {}")
        }

        classBuilder.append("}")

        return KtPsiFactory(project).createClass(classBuilder.toString())
    }

    private fun getOverridedName(element : KtNamedDeclaration, defaultName : String) =
            element.docComment?.getDefaultSection()?.findTagByName("pcRename")?.getContent() ?: defaultName

    private fun getMethodFrom(entity : KtNamedFunction) : MethodEntity? {

        val name = entity.name?.let { getOverridedName(entity, it) } ?: return null

        val parameters = entity.valueParameters.mapNotNull { it.name }.toList()

        return MethodEntity(name, parameters, entity)
    }

    override fun findAllClasses(psiFile: PsiFile): List<ClassEntity> {

        assert(psiFile.language.id == language)

        val ktFile = psiFile as KtFile

        val ktClass = PsiTreeUtil.collectElementsOfType(ktFile, KtClass::class.java)

        return ktClass.mapNotNull { clazz ->
            val className = clazz.name
            if(className != null){
                val functions =  clazz.getBody()
                        ?.declarations
                        ?.filterIsInstance<KtNamedFunction>()
                        ?.mapNotNull { getMethodFrom(it) }
                        ?.toList() ?: listOf()
                ClassEntity(getOverridedName(clazz, className), functions, clazz)
            }else{
                null
            }
        }.toList()
    }

    override val language = "kotlin"
}