package com.iyakovlev.parallelcode.plugincore.languagesupport

import com.intellij.openapi.project.Project
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import com.intellij.psi.util.PsiTreeUtil
import com.iyakovlev.parallelcode.plugincore.languagesupport.ClassEntity
import com.iyakovlev.parallelcode.plugincore.languagesupport.LanguageService
import com.iyakovlev.parallelcode.plugincore.languagesupport.MethodEntity
import com.jetbrains.python.psi.*

class PythonService : LanguageService {

    private fun List<String>.parametersToString() =
            if (isNotEmpty()) "self, " + reduce { acc, s -> "$acc, $s" } else "self"

    override fun generateMethod(entity: MethodEntity, project: Project, into: PsiElement) {

        into as PyClass

        val newMethod = PyElementGenerator
                .getInstance(project)
                .createFromText(null, PyFunction::class.java, "def ${entity.name}(${entity.parameters.parametersToString()}): pass")

        val statements = into.children
                .filterIsInstance<PyStatementList>()
                .firstOrNull()

        if(statements != null){
            if(statements.children.size == 1 && statements.children[0] is PyPassStatement){
                statements.children[0].replace(newMethod)
            }else{
                statements.add(newMethod)
            }
        }
    }

    override fun generateClass(entity: ClassEntity, project: Project): PsiElement {

        val classBuilder = StringBuilder()
        classBuilder.append("class ${entity.name}:\n")


        for(method in entity.methods){
            classBuilder.append("  def ${method.name}(${method.parameters.parametersToString()}): pass")
        }

        if(entity.methods.isEmpty()){
            classBuilder.append("  pass")
        }

        return PyElementGenerator
                .getInstance(project)
                .createFromText(null, PyClass::class.java,  classBuilder.toString())
    }

    private fun getOverridedName(element : PyDocStringOwner, defaultName : String) =
            element.structuredDocString?.getParamType("pcRename") ?: defaultName //have no idea how to make it better that type without parsing comment line

    private fun getMethodFrom(entity : PyFunction) : MethodEntity? {

        val name = entity.name?.let { getOverridedName(entity, it) } ?: return null

        val parameters = entity.parameterList.parameters.drop(1).mapNotNull { it.name }.toList()

        return MethodEntity(name, parameters, entity)
    }

    override fun findAllClasses(psiFile: PsiFile): List<ClassEntity> {

        assert(psiFile.language.id == language)

        val pyFile = psiFile as PyFile

        val pyClass = PsiTreeUtil.collectElementsOfType(pyFile, PyClass::class.java)

        return pyClass.mapNotNull { clazz ->
            val className = clazz.name
            if(className != null){

                val functions = clazz.methods.mapNotNull { getMethodFrom(it) }.toList()

                ClassEntity(getOverridedName(clazz, className), functions, clazz)

            }else{
                null
            }
        }.toList()
    }

    override val language = "Python"
}