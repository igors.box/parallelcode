package com.iyakovlev.parallelcode.plugincore.languagesupport

import com.intellij.openapi.project.Project
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile

data class ClassEntity(val name : String, val methods : List<MethodEntity>, @Transient val element : PsiElement?) : java.io.Serializable
data class MethodEntity(val name : String, val parameters : List<String> ,@Transient val element : PsiElement?) : java.io.Serializable

interface LanguageService{
    fun findAllClasses(psiFile : PsiFile) : List<ClassEntity>
    fun generateClass(entity: ClassEntity, project : Project): PsiElement
    fun generateMethod(entity: MethodEntity, project : Project, into : PsiElement)
    val language : String
}