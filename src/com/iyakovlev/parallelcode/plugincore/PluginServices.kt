package com.iyakovlev.parallelcode.plugincore

import com.iyakovlev.parallelcode.plugincore.languagesupport.ClassEntity
import com.iyakovlev.parallelcode.plugincore.languagesupport.MethodEntity
import com.intellij.openapi.command.WriteCommandAction
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.progress.ProgressManager
import com.intellij.openapi.progress.Task
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiDocumentManager
import com.intellij.psi.PsiElement

class PluginServices(private val project : Project) {

    val projectManipulator = DataManipulator(project)

    fun insertCodeEntity(entity : ClassEntity){

        val editor = FileEditorManager.getInstance(project).selectedTextEditor

        if(editor != null){
            val psiFile = PsiDocumentManager.getInstance(project).getPsiFile(editor.document)

            if(psiFile != null){
                val clazz = projectManipulator.generateClass(entity, psiFile)

                if(clazz != null){
                    WriteCommandAction.runWriteCommandAction(project) {
                        psiFile.add(clazz)
                    }
                }
            }
        }
    }

    fun insertCodeEntity(into : PsiElement, method : MethodEntity){

        WriteCommandAction.runWriteCommandAction(project) {
            projectManipulator.generateMethod(method, into)
        }

        FileEditorManager.getInstance(project).openFile(into.containingFile.virtualFile, true)
    }

    fun <T> modalOperation(operation : (statusText : (String) -> Unit) -> T) : T {

        val getter = object : Task.Modal(project, "Working...", false) {

            var result : T? = null

            override fun run(p0: ProgressIndicator) {
                result = operation { p0.text = it }
            }
        }
        ProgressManager.getInstance().run(getter)

        return getter.result!!
    }

    fun goto(element: PsiElement) {
        FileEditorManager.getInstance(project).also {
            it.openFile(element.containingFile.virtualFile, true)
        }
    }
}