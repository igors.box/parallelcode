package com.iyakovlev.parallelcode.plugincore.interop

data class LocalId(val projectId : String, val projectName : String){
    companion object {

        fun tryParse(value : String) =
                value.split('|').let { if(it.count() == 2) LocalId(it[0], it[1]) else null }
    }

    override fun toString() = "$projectId|$projectName"
}
