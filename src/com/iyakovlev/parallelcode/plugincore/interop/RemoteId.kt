package com.iyakovlev.parallelcode.plugincore.interop

data class RemoteId(val port : Int, val objectName : String, val id : LocalId){
    override fun toString(): String = id.projectName
}