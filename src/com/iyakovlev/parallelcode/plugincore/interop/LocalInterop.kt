package  com.iyakovlev.parallelcode.plugincore.interop

import com.iyakovlev.parallelcode.plugincore.languagesupport.ClassEntity
import com.iyakovlev.parallelcode.plugincore.DataManipulator
import com.intellij.openapi.Disposable
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.Disposer
import com.intellij.util.messages.Topic
import java.util.*
import java.util.Collections.synchronizedList

private interface DataRetrieverTopic {
    fun getIDs(result : MutableList<LocalId>)
    fun getInstanceClasses(id: LocalId, result : MutableList<ClassEntity>)
}

class LocalInterop(project : Project) : DataRetrieverTopic, Disposable {

    private val id = LocalId(UUID.randomUUID().toString(), project.name)

    private val localDataRetriever = DataManipulator(project)

    private val busConnection = ApplicationManager.getApplication().messageBus.connect()

    init {
        busConnection.subscribe<DataRetrieverTopic>(TOPIC, this)
        Disposer.register(project, this)
    }

    override fun dispose() {
        busConnection.disconnect()
    }

    override fun getIDs(result : MutableList<LocalId>) {
        result.add(id)
    }

    override fun getInstanceClasses(id: LocalId, result : MutableList<ClassEntity>) {
        if(this.id == id){
            result.addAll(localDataRetriever.retrieveLocalData())
        }
    }

    companion object {

        private val TOPIC = Topic.create<DataRetrieverTopic>("DataRetrieverTopic", DataRetrieverTopic::class.java)

        private val syncObject : DataRetrieverTopic = ApplicationManager.getApplication().messageBus.syncPublisher(TOPIC)

        val applicationIDs : List<LocalId> get() {
            val result = synchronizedList(mutableListOf<LocalId>())
            syncObject.getIDs(result)
            return result
        }

        fun getClasses(requestId: LocalId) : List<ClassEntity>{
            val result = synchronizedList(mutableListOf<ClassEntity>())
            syncObject.getInstanceClasses(requestId, result)
            return result
        }
    }
}