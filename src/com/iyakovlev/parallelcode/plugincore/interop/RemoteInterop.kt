package com.iyakovlev.parallelcode.plugincore.interop

import com.iyakovlev.parallelcode.plugincore.languagesupport.ClassEntity
import com.intellij.openapi.Disposable
import de.root1.simon.Registry
import de.root1.simon.Simon
import de.root1.simon.annotation.SimonRemote
import java.io.*
import java.util.*
import java.util.function.Supplier

@SimonRemote
class RemoteServer : java.util.function.Function<String, ByteArray?>, Supplier<List<String>>, Disposable {

    private val currentServerName = "$parallelRemoteTagPrefix${UUID.randomUUID()}"

    //INTERFACE METHODS
    //Getting all names
    override fun get(): List<String> = LocalInterop.applicationIDs.map { it.toString() }.toList()
    //Getting remote data
    override fun apply(t: String): ByteArray? {

        val localId = LocalId.tryParse(t) ?: return null

        val target = LocalInterop.getClasses(localId)

        ByteArrayOutputStream().use {
            ObjectOutputStream(it).use {
                it.writeObject(target)
                it.flush()
            }
            return it.toByteArray()
        }
    }

    companion object {
        const val parallelRemoteTagPrefix = "__ParallelRemote__"

        private val publications get() =
            Simon.searchRemoteObjects(100)
                .filter { it.remoteObjectName.startsWith(parallelRemoteTagPrefix) }

        val remoteIds get() =
            publications.map {
                val names = (Simon.createNameLookup("127.0.0.1", it.port)
                        .lookup(it.remoteObjectName) as? Supplier<*>)
                        ?.get() as? List<*>

                names
                        ?.filterIsInstance<String>()
                        ?.mapNotNull { LocalId.tryParse(it) }
                        ?.map { local -> RemoteId(it.port, it.remoteObjectName, local)}
                        ?:listOf()
            }.flatten().toList()

        fun getRemoteData(id : RemoteId) : List<ClassEntity> {
            val suppliedData = (Simon.createNameLookup("127.0.0.1", id.port)
                    .lookup(id.objectName) as? java.util.function.Function<String, *>)
                    ?.apply(id.id.toString()) ?: return listOf()

            if(suppliedData is ByteArray){
                ByteArrayInputStream(suppliedData).use {
                    ObjectInputStream(it).use {
                        return (it.readObject() as? List<*>)
                                ?.mapNotNull { it as? ClassEntity }
                                ?.toList() ?: listOf()
                    }
                }
            }
            return listOf()
        }
    }

    private val serverInstance = tryCreateRegistry()

    override fun dispose() {
        serverInstance.unpublish(currentServerName)
        serverInstance.unbind(currentServerName)
        serverInstance.stop()
    }

    private fun tryCreateRegistry() : Registry{

        for(i in 1..3){
            val freePort = (6556..6666).subtract(publications.map { it.port }).firstOrNull() ?: throw IllegalStateException()

            try{
                return Simon.createRegistry(freePort).also { it.start() }
            }catch (e : IOException){}
        }
        throw IllegalStateException()
    }

    init {
        serverInstance.bindAndPublish(currentServerName, this)
    }
}
