package com.iyakovlev.parallelcode

import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.application.PreloadingActivity
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.project.Project
import com.intellij.openapi.project.ProjectManager
import com.intellij.openapi.project.VetoableProjectManagerListener
import com.intellij.openapi.util.Disposer
import com.iyakovlev.parallelcode.plugincore.interop.LocalInterop
import com.iyakovlev.parallelcode.plugincore.interop.RemoteServer

class PluginPreloadActivity : PreloadingActivity() {


    override fun preload(indicator: ProgressIndicator) {

        val application = ApplicationManager.getApplication()
        Disposer.register(application, RemoteServer())

        val projectManager = ProjectManager.getInstance()

        for(project in projectManager.openProjects){
            Disposer.register(project!!, LocalInterop(project))
        }

        projectManager.addProjectManagerListener(object : VetoableProjectManagerListener{
            override fun canClose(p0: Project) = true
            override fun projectOpened(project: Project?) {
                Disposer.register(project!!, LocalInterop(project))
            }
        })
    }
}